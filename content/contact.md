---
title: "Contact"
date: 2021-07-28T15:56:19+05:30
draft: false
---

Mail: kskarthik [at] disroot [dot] org

- GPG Key: [SaiKarthik.asc](../SaiKarthik.asc)

- GPG Fingerprint: 2E68 0E6E 792B 9218 C567 CF31 F5B9 A961 BF6E AF0E

Matrix ID: @kskarthik:poddery.com
