---
title: "Hacking on Movim Xmpp"
date: 2023-06-11T10:10:30+05:30
draft: false
keywords: ["movim", "xmpp", "github actions", "docker"]
tags: ["movim"]
---

> Movim is a federated blogging and chat platform that acts as a web frontend for the XMPP protocol.

The project's web UI for xmpp (jabber) is really good & also mobile friendly. I have observed that the project does not have a CI/CD setup so far & thought of adding one. I have added basic CI/CD work for the movim repository on github with github actions. For every commit & pull request to master branch, the app is built in the pipeline & if successful, a docker image is uploaded to movim's repositoy on docker hub.

The team had given me feedback during the implementation & i had a chance to explore new concepts in docker image building & some bash scripting as well! I have also planned to automate the docker image building for stable releases, But the team preferred to do it manually.

Summary of my commits:

- movim repo: https://github.com/movim/movim/commits?author=kskarthik
- movim docker: https://github.com/movim/movim_docker/commits?author=kskarthik
