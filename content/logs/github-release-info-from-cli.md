---
title: "Github Release Info From CLI"
date: 2023-06-29T11:02:19+05:30
draft: false
keywords: ["github", "api", "curl", "jq"]
tags: ["cli", "github", "curl", "jq"]
---

It's very easy to keep track of a github project's release from the linux CLI using just `curl` & `jq`.

An example of getting latest release from the razorpay's ifsc repo:

```
curl -s https://api.github.com/repos/razorpay/ifsc/releases?per_page=1  | jq -r '.[0].name'
```
