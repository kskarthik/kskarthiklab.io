---
title: "Eco Friendly Software"
date: 2024-05-19T13:11:58+05:30
draft: false
keywords: ["eco friendly software"]
tags: ["eco-software"]
---

Since the end of year 2022, I have mostly switched to using free and opensource programs which use less computer resources.

One of the main reason to switch is that these programs:
- improve my workflow
- extend the life span of my hardware
- can be highly customizable
- config can be saved in a text file which is easy to backup

List of some programs i use:

- Window manager: [Sway](https://swaywm.org)
- Email: [aerc](https://aerc-mail.org)
- Editor/IDE: [Neovim](https://neovim.io)
- Bluetooth: [bluetuith](https://github.com/darkhz/bluetuith)

Today, I felt surprised that there is an initiative by the German government's environment agency
to certify software products as "Blue Angel". The three main categories of the Blue Angel award criteria for desktop software are:

(A) Resource & Energy Efficiency

(B) Potential Hardware Operating Life

(C) User Autonomy


> The first FOSS computer program to be certified "Blue Angel" is KDE's Okular!

Personally, I believe that all Linux FOSS CLI/TUI programs & window managers are eligible for the "Blue Angel" certification :laughing:

KDE has a dedicated page explaining all aspects of the programme [here](https://eco.kde.org/handbook/)

