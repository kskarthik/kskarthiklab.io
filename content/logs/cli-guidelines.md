---
title: "Best Practices While Building a CLI program"
date: 2023-10-29T20:14:36+05:30
draft: false
keywords: ["cli", "unix"]
tags: ["cli", "linux"]
---

I have recently came across this amazing website https://clig.dev/ which contains very
useful guidelines for developers who wish to create a new CLI tool.

Not just that, They also curated a list of the popular CLI libraries for
many programming languages.

I found this website very resourceful to code my first CLI tool [go-ifsc](https://gitlab.com/kskarthik/go-ifsc)
which is built in Golang.
