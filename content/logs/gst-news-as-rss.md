---
title: "Gst News as RSS Feeds"
date: 2024-11-26T12:36:56+05:30
draft: false
keywords: ["gstfeed"]
tags: ["gst", "rss"]
---
"Over the weekend, I built a simple yet useful tool called [gstfeed](https://github.com/kskarthik/gstfeed), which converts news and updates from the Indian GST portal into RSS (Atom) feeds. This makes it easy to integrate the latest information into various tools, services, and instant messaging bots. To help you get started, I've also set up a demo feed, which you can access through the link in the repository. Feel free to try it out and explore the possibilities!"

I chose to use Deno for this tool as i have been following the project for a while now. Particularly the v2 release was well received by the community. The developer experience was impressive, with built-in package management, linting, formatting support. Typescript is supported out of the box! The selling point for me is the ability to compile a project into a static binary which includes the project files, dependencies & the deno runtime. Thus, making deployments & distribution easier.

