---
title: "Why Debian?"
date: 2023-10-14T22:43:52+05:30
draft: false
keywords: ["debian"]
tags: ["debian"]
---

I have recently read a blog post describing some choices debian made to provide an high quality operating system, both technical & ideological aspects. I enjoyed reading it.

Personally, I like the debian social contract which prioritizes openness & users, which a lot of linux operating systems lack. We saw the consequences users/companies/projects have to face, because of the way how redhat `flipped` their policies regarding disclosing the source code of packages.

I have some thoughts on debian's package policy though. Below are some sentences i am quoting from the blog post:

> If Debian weren’t self-contained, it would be at the mercy of any of the tens of thousands of packages it has, and all their dependencies, being available when an urgent security fix needs to be released. This is not acceptable to Debian, and so Debian chooses to do the work of packaging all dependencies.

Technially, This is a very good choice & reflects the project's upmost concern on security & stability.

> That means, of course, that for Debian to package something can be a lot of work.

This is also true. Not just debian, But GNU/Guix, Another amazing operating system by the GNU project, has similar packaging policy.

I have packaged/updated a [few](https://qa.debian.org/developer.php?email=kskarthik@disroot.org) debian packages in the past. It's time taking, particularly with language ecosystems like node, ruby, Go. For example, packaging Gitlab requires a [dedicated team](https://opencollective.com/debian-gitlab) of people, since it has hundreds of packages to maintain.

My thought is whether we can find a good balance between package security & reduction in packaging time by reducing the number of dependencies to package. I am no expert in this aspect. But, There are many experts in the debian community who could have a good idea.

Blog Link: https://blog.liw.fi/posts/2023/debian-reasons/
