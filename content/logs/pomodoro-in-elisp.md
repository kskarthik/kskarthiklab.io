---
title: "A simple Pomodoro timer in Elisp"
date: 2023-12-02T21:04:36+05:30
draft: false
keywords: ["emacs", "pomodoro"]
tags: ["emacs", "pomodoro", "elisp"]
---

This is a simple Pomodoro timer which i wrote some time ago in elisp while using doom emacs!

The code is based on systemcrafters pomodoro example. I have customized it a bit more to support pause & resume! :grin:

```lisp
;; Based on https://systemcrafters.cc/emacs-shorts/pomodoro-timer/
(defun my/pomodoro-timer ()
  "A simple pomodoro timer set for 30 min. If timer is running, Calling this function will pause/resume it"
  (interactive)
  ;; pause / resume if timer is running
  (if (not (equal (org-timer-value-string) "0:00:01 "))
      (org-timer-pause-or-continue)
  ;; supported sound formats: https://www.gnu.org/software/emacs/manual/html_node/elisp/Sound-Output.html
    (setq! org-clock-sound  (concat doom-private-dir "attention.wav"))
    (org-timer-set-timer 30) ;; 30 minutes
    )
)
```
