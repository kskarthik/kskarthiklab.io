---
title: "Exploring Guix"
date: 2021-05-16T21:50:06+05:30
draft: false
keywords: ['gnu',"guix"]
tags: ["guix"]
---
I have recently got to know about [GNU Guix](https://guix.gnu.org) GNU/Linux distribution. It is unlike other traditional linux distributions introduced new pardigm of package management, system configuration & many more interesting features.

It is inspired by NixOS. But, entirely implemented including an init system (GNU shepherd) in a programming language called [Guile](https://www.gnu.org/software/guile/) which belongs to lisp family & a dialect of scheme.

I have flashed the OS on my x230 laptop. Installation went smooth. It's a CLI installer. I have selected the GNOME desktop environment which is at v3.36 at the time of writing this article. 

Currently, I have issues with low speaker volume, touchpad tap to click is missing. I still have to figure out how to fix those.

The OS is kind of rolling release. To update the repositories we have to run `guix pull` & then to upgrade the packages `guix upgrade`

These commands work without `sudo` which is great! This is a nice feature in guix.

I still have to explore a lot & will write about it another post!

