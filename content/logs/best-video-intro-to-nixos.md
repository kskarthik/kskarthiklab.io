---
title: "Best Video Intro to Nixos"
date: 2024-07-11T19:51:20+05:30
draft: false
keywords: ["nixos", "intro"]
tags: ["nixos"]
---

I just saw a youtube video which gives clear & concise introduction to NixOS in 15 minutes.

Covers the do & dont's as well. Worth a watch!

Video link: https://www.youtube.com/watch?v=CwfKlX3rA6E
