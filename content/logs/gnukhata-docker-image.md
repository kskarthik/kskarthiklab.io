---
title: "GNUkhata Docker Image"
date: 2020-01-22T15:46:48+05:30
draft: false
tags: ["gnukhata", "docker", "debian"]
keywords: ["gnukhata latest debian docker image dockerhub"]
---
I made this image as i couldn't find any recent Dockerfiles for GNUKhata. So, I forked an old Dockerfile from GNUKhata's gitlab repository & rebased it to debian buster.

The current image installs GNUKhata `v6.50`. I have intention to support future releases too!

Build instructions are available at https://gitlab.com/kskarthik/gnukhata-docker
