---
title: "Emacs: Trying to upgrade packages asynchronously"
date: 2023-12-13T10:26:25+05:30
draft: false
keywords: ["elisp", "async"]
tags: ["elsip", "async"]
---
As i started configuring emacs from scratch, I have chosen to use the default package manager `(use-package)`

When i tried to refresh & upgrade packages, I noticed that it blocks the emacs UI until the operation is finished.

I wanted to move this work to the background without blocking the UI. When browsing melpa,
I found a package named [async](https://melpa.org/#/async) which provides a couple of functions
to do write async code.

```lisp
(defun async-upgrade-packages ()
  "Update packages to latest version without blocking Emacs."
  (interactive)
  (message "Upgrading packages in the background 🚀")
  (async-start (lambda ()
		 (package-refresh-contents)
		 (package-upgrade-all))
	       (lambda (result)(message result))))
```

The above code snippet does a decent job, But i have to learn how to handle errors
in async way, As the `(package-refresh-contents)` throws an error if failed to fetch packages.

Thanks to the user [(save-lisp-and-die)](https://contrapunctus.codeberg.page/) in the xmpp [lisp](xmpp:lisp@conference.a3.pm) room, who helped me with understanding the async package.
