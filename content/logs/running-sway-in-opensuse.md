---
title: "Running Sway in openSUSE"
date: 2024-12-21T13:55:10+05:30
draft: false
keywords: ["sway", "opensuse"]
tags: ["sway", "opensuse"]
---

openSUSE tumbleweed has a good collection of sway window manager & it's surrounding tools. There are a couple of ways to install sway & related packages

### Traditional way

If you wish to handpick your sway setup, you can download individual packages via zypper.

```bash
sudo zypper in sway waybar
```

### Via Patterns

A pattern is nothing but a collection of related packages which could be installed via single keyword. Sway also has such a pattern, which can be installed via the command below. This list of packages this patterns installs are defined [here](https://build.opensuse.org/projects/X11:Wayland/packages/patterns-sway/files/patterns-sway.spec?expand=1).

```sh
sudo zypper in patterns-sway-sway
```

### openSUSEway

This is an opinionated sway desktop environment which ships with a bunch of pre configured packages & settings. More details & install instructions can be found in the [openSUSEway wiki](https://en.opensuse.org/Portal:OpenSUSEway)  
