---
title: "Minimal Gnome Install on Debian"
date: 2021-07-07T23:21:28+05:30
draft: false
keywords: [gnome minimal install in debian"]
tags: ["debian", "gnome"]
---

I do not like the extra apps which ship with debian gnome live iso. So, I found  a better way to achieve a minimal GNOME experience. 

### The process:

* First, Download the [debian net install iso](https://cdimage.debian.org/cdimage/weekly-builds/amd64/iso-cd/debian-testing-amd64-netinst.iso) 

* Flash the iso to an usb stick using the program of your choice. I use `dd`

* Begin the install process. In the later stage of installer, You will come up with desktop environment selection menu, Skip that stage & complete the install 

* reboot & login. After logging in, Install the `task-laptop` package using `sudo apt install task-laptop`. If you have a desktop, Then `sudo apt install task-desktop` should work

* Now, install the `gnome-core` meta package with `sudo apt install gnome-core`

* Reboot the pc

Voila! Now you are greeted with gdm login screen & your gnome desktop setup is done. Enjoy the minimal gnome environment & install the additional apps you need.

