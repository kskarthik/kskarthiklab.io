---
title: "Monopriv: A New Theme for Hugo"
date: 2019-12-12T15:31:59+05:30
draft: false
tags: ["hugo", "theme", "monopriv"]
keywords: ["monopriv hugo theme", "privacy hugo theme", "bootstrap hugo theme"]
---

I have recently created this theme for hugo, This theme is based on bootstrap
framework. I have only used the built-in bootstrap components to design the whole
theme.

### Features

* Minimal blogging focused theme
* No javascript required to work
* RSS Feeds
* Monospace font
* Site Search (_Powered by DuckDuckG0_)

Please feel free to use, add & contribute changes to this theme! It's under MIT Licese.

Link https://gitlab.com/kskarthik/monopriv
