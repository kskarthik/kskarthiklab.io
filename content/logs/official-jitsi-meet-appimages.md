---
title: "Official Jitsi Meet AppImages"
date: 2021-01-31T23:24:25+05:30
draft: false
keywords: ["jitsi meet official appimage linux"]
tags: ["jitsi", "appimage"]
---
Jitsi meet is also available as appimages for linux, which is not listed on official download page for unknown reasons. So, I am providing the link below to download 

Download link > https://github.com/jitsi/jitsi-meet-electron/releases

Enjoy !