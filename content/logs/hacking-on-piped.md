---
title: "Hacking on Piped"
date: 2022-06-25T22:41:00+05:30
draft: false
keywords: ["piped"]
tags: ["piped"]
---

> Piped is a lightweight self-hostable libre youtube front-end

I have been using [Piped](https://github.com/TeamPiped/Piped) since a couple of months & it's an awesome application!

The project has two parts:

- Back-end uses NewPipe Extractor library & provides a REST API
- Front-end is a Vuejs based progressive web app with mobile friendly user interface

I have looked into the front-end code & found a few areas which can be improved. So, I stated with improving the
chapters layout & making it more mobile friendly & visually good. Submitted a PR & it was accepted!

Later, i found out that the `shorts` thumbnails were not aligned properly with rest, I picked up that task & tried to
fix them. I got a few suggestions from the developer [Kavin](https://github.com/fireMasterK), We both brainstormed on it & found a solution together!

After the Initial refractor of the chapters section, We got feedback from the community that they wanted a youtube style chapters layout on the desktop. With a few trial & errors, I added that feature as well & kavin was also very happy with the layout, I pushed the PR & with some small final changes from kavin, It was merged as well :tada:

### Conclusion:

- I am very happy to take part in development of a program which i use daily
- Hacking on Piped was really fun! and i got to learn many new things in the process
- Kavin was very friendly & patient. Above all, He is also aligned with the free software ideology

### Links

- Piped Instance: https://piped.kavin.rocks
- Git Repository https://github.com/TeamPiped/Piped
- My [commits](https://github.com/TeamPiped/Piped/commits?author=kskarthik)
