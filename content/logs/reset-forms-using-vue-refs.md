---
title: "Reset HTML Forms Using Vue Refs"
date: 2023-08-11T11:26:16+05:30
draft: false
keywords: ["vue", "forms", "reset", "refs"]
tags: ["vue"]
---

Vue refs come handly when we want to access DOM elements directly. You can read more about vue refs [here](https://vuejs.org/guide/essentials/template-refs.html)

In this case, we want to reset a form. So, let's assign a html attribute named ref to a `<form>` tag

A simple example:

```html
<form ref="myForm">
    <input></input>
</form>
```

Now we can apply DOM methods to an element like `reset()` on this form by calling `this.$refs.myForm.reset()`
