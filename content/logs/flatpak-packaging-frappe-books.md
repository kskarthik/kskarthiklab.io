---
title: "Flatpak Packaging: frappe Books"
date: 2024-10-20T22:32:01+05:30
draft: false
keywords: ["flatpak", "frappe books"]
tags: ["flatpak", "linux"]
---

Frappe Books is an open source accounting desktop application. I have packaged [frappe books](https://frappe.io/books) as a [flatpak](https://flathub.org/apps/io.frappe.books), I wish to write my experience of packaging & submitting the app to the flathub portal.

# Motivation

I like frappe books & how it simplifies accounting for small business use cases. It is built using the popular electron application framework  & they distribute binaries for windows, mac & linux (deb, rpm, appimage). For linux, the problem i found is that the user has to manually keep track of latest releases & download the new binary & install. Using flatpak to distribute the app on flathub will ease the installation & updating of packages for many users. Also the application's visibility will also increase since the flathub is a popular place to find flatpaks for all sort of categories.

# Building

To build an application as a flatpak, there is a tool named `flatpak-builder` which helps with the build process. It also has the ability to install the built package locally after a successful build. So the developer can test the app locally & make sure the app is working as expected. Flatpak's official [documentation](https://docs.flatpak.org/en/latest/) is a great place to learn about the build process. There is also a dedicated section about how to build electron based apps from source.

I have followed the instructions & tried build the app from source initially. But i came across some errors regarding npm package vendoring path, which i was unable to resolve even after going through the docs. Being the first time could be a reason. Then i tried to use the offical rpm archive by the project & was able to build & launch the app locally! :tada: 

Infact i have observed that most of the electron apps submitted to the flathub are packaged in this fashion. But it comes with it's own drawbacks. Flathub supports `x86_64` & `aarch64` architectures. If we define instructions as building from source in the flatpak manifest, Flathub will build & publish the app for both the architectures automatically! This is a great advantage from a developer perspective. Since, i extracted the binary rpm & installed it inside flatpak, I missed this facility. But i am working on achieving multi arch falthub builds for frappe books. It helps the users of `aarch64` (eg: raspberry pi) easy to install the app as the upstream does not provide official binaries for it.

Sandboxing is the main selling point of flatpak. The developer, as well as the user can restrict the app from accessing system resources (network, file system, IPC & many more). I tried to confine the app limited access to the host & gave it only the necessary permissions to work as it intended to be.

# Submitting to flathub

After i made sure the app is built successfully & running as expected locally, I followed the flathub's [submission process](https://docs.flathub.org/docs/for-app-authors/submission/) to publish the app on flathub. The process continued for a few days during which my submission was reviewed & the team provided feedback on how to improve the quality of the package. 

I am greatly thankful to [@bbhtt](https://github.com/bbhtt) who has patiently guided me throughout the process with thier valuable feedback & suggestions. I have learn a lot more aspects of flatpak packaging during this review process. After passing all the checks, the package was approved for inclusion into flathub!

# Links
- Frappe Books Source: https://github.com/frappe/books
- Frappe Books Flatpak: https://flathub.org/apps/io.frappe.books
- Flatpak Build Manifest: https://github.com/flathub/io.frappe.books


