---
title: "Easy Way to Know Your Public IP Address From Terminal"
date: 2024-06-14T12:44:54+05:30
draft: false
keywords: ["changeme"]
tags: ["linux", "curl"]
---

I have learn recently from my colleague about this new way to easily know your public IP Address
from your terminal. Requirements are `curl` & `jq` (optional).


The response in this format is quiet useful for scripting usecases.

The url responds with a json object which contains your location, ISP, IP address etc... (response data below is masked for obvious reasons)

```sh
curl -s ipinfo.io

{
  "ip": "....",
  "hostname": "....",
  "city": ".......",
  "region":  "....."
  "country": ".....",
  "loc": "......",
  "org": "......",
  "postal": "......",
  "timezone": "....",
  "readme": "....."
}

```

Or Just add the below alias to your `~/.bashrc` to keep it handy

With `jq`

```sh
alias myip="curl -s ipinfo.io | jq -r '.ip'"
```

Without the need of `jq`

```sh 
curl -s ipinfo.io | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
```
