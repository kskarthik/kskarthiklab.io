---
title: "Automount USB drives in Linux"
date: 2024-01-31T14:05:47+05:30
draft: false
keywords: ["automount", "usb", "linux"]
tags: ["linux", "usb", "flatpak"]
---

As i started using flatpaks for all graphical applications in my new desktop setup, I came across an issue
where the usb disks are not automatically detected in my file manager (Dolphin) inside flatpak. I was using nautilus / pcmanfm installed via native package manager (apt) before & they used to do this job by default.

As i started to search for the solution on the web & asking [llama2](https://llama2.ai), i have learnt that the package `udisks2` can auto mount the external disks.

> The following guide assumes you are using debian, Some commands might change depending on your distribution.


Let's start by installing the package first.
```sh
sudo apt install udisks2
```

Then, We can use the `udisksctl` command to define which disks to mount. You can find the device id which you wish to mount, using `fdisk -l` command.

For example, I want to mount the usb drive with device id `/dev/sdb1` 

```sh
sudo udisksctl mount -b /dev/sdb1
```

If you want all the disks to get automatically mounted when inserted, You can add the following lines to the `udisks2` config file located in `/etc/udisks2/udisks2.conf`

```sh
[Automount]
enabled=true
defaults=ro
```

Then, restart the `udisks2` service with `systemctl restart udisks2.service`. Now plug your usb drive & watch it appear in your file manager.

This was a fun learning experience & helped me understand some of the limitations of flatpaks. And, do let me know if there is much simple way!
