---
title: "Merging Multiple Vcards On Linux"
date: 2019-10-20T12:49:01+05:30
draft: false
tags: ["linux"]
keywords: ["merging multiple vcards on linux"]
---
Recently, I needed to transfer contacts from an old samsung feature phone to an android smartphone. The issue i faced was that the samsung feature phone backups all contacts in a folder & organises them as one vcf file per contact, But android's vcard saves all contacts in a single file. So, I found a way of merging all these indvidual vcards into single vcard by simply using linux command.

`cd` into the folder containing all vcards

then

`cat * >> contacts.vcf`

That's it ! Now all the contacts are merged into the `contactList.vcf` file

#### *What's happening in the background?*
Let's look at the anatomy of a single vcard entry

{{< highlight bash >}}
BEGIN:VCARD
VERSION:2.1
N;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:;Best Offer;;;
TEL;CELL:505
END:VCARD
{{< /highlight >}}

Every vcard entry begins with `BEGIN:VCARD` & ends with `END:VCARD` lines

The command `cat * >> contacts.vcf` concatenates all the contents of individual vcard files to a single `contacts.vcf` file which you can import in android.
