---
title: "Gnukhata Alpine Docker Image"
date: 2020-04-29T14:15:50+05:30
draft: false
tags: ["gnukhata","alpine"]
keywords: ["gnukhata alpine docker image docker hub", "gnukhata latest docker alpine image "]
---

I have created this docker image based on alpine linux 3.11. It Consumes 50% less disk space than my previous debian buster based image.

> This image is also available on docker hub.

Link: https://hub.docker.com/r/kskarthik/gnukhata/tags

Dockerfile: https://gitlab.com/kskarthik/gnukhata-docker



