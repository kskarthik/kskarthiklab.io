---
title: "Fixing the hostname resolving error while using sudo"
date: 2024-03-08T09:36:57+05:30
draft: false
keywords: ["hostname"]
tags: ["linux", "hostname"]
---

When we change the linux system's hostname via `hostnamectl hostname` command, It's not reflected in the `/etc/hosts` file which causes `sudo` to show a warning as shown below.

```sh
sk@sun:~$ sudo apt update
sudo: unable to resolve host sun: Name or service not known
Hit:1 http://deb.debian.org/debian sid InRelease
Reading package lists... Done
Building dependency tree... Done
...
```

Here's how to properly set a hostname.

```sh
# sets the hostname to sun
sk@moon:~$ sudo hostnamectl hostname sun
# replaces the old hostname to new one in /etc/hosts
sk@sun:~$ sudo sed -i 's/moon/sun/' /etc/hosts
```

That's it!
