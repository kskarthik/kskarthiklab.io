---
title: "About"
date: 2019-04-19T21:37:58+05:30
type: "page"
---

Hello Stranger! :wave:

I am a [free software](https://www.fsf.org/about/what-is-free-software) [hacker](https://en.m.wikipedia.org/wiki/Hacker_ethic).

:drum: I wish to be a generalist & have a wide range of interests, which include but not limited to:

- Full stack web dev
- DevOps & Automation
- System administration
- Blockchain
- AI

Currenly active in the [openSUSE](https://en.opensuse.org/User:Kskarthik) project.

#### **MY PROJECTS**

- [gstfeed](https://github.com/kskarthik/gstfeed) - Converts news & updates from the Indian GST portal as RSS feeds which can be utilized in various integrations (feedreaders, bots).

- [indian-fincodes-api](https://github.com/kskarthik/indian-fincodes-api) - A REST API server made using docker & meilisearch to search Indian bank info, pincodes & HSN/SAC codes. The build, test & deployment process is entirely automated using github actions.

- [go-ifsc](https://gitlab.com/kskarthik/go-ifsc) - A CLI & REST API sever to search the details for Indian banks

- My [Debian Packages](https://qa.debian.org/developer.php?email=kskarthik@disroot.org)

- [Guix Package API](https://gitlab.com/kskarthik/guix-packages-api) - REST API server for GNU Guix Packages, Made using Deno

- [Sailfish OS for Mozilla Flame](https://together.jolla.com/question/155313/alpha-sailfishos-v2056-rom-for-mozilla-flame/)

- [postmarketOS for Sony Z3C](<https://wiki.postmarketos.org/wiki/Sony_Xperia_Z3C_(sony-aries)>)

HUGO THEMES

- [monopriv](https://themes.gohugo.io/monopriv/): A lightweight bootstrap blogging theme

- [resto-hugo](https://themes.gohugo.io/resto-hugo/): Port of resto theme to hugo

FIREFOX ADD-ON'S

- [Wikimedia Input Tools](https://addons.mozilla.org/en-US/firefox/addon/wikimedia-input-tools/) - Enables the user to type in 120+ languages in firefox web browser

- [Indic Input](https://addons.mozilla.org/en-US/firefox/addon/indic-input/) - Enables the user to write in serveral Indian languages directly on webpages

#### **CONTACT**

- Email / XMPP (Jabber): `kskarthik at disroot dot org`

- My [GPG Key](../SaiKarthik.asc) | Fingerprint: `2E68 0E6E 792B 9218 C567 CF31 F5B9 A961 BF6E AF0E`

Links:

- [Gitlab](https://gitlab.com/kskarthik)
- [Github](https://github.com/kskarthik)
- This website's [source code](https://gitlab.com/kskarthik/kskarthik.gitlab.io)
