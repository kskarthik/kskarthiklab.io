/*
  * Name: Crypto Sim
  * Author: Sai Karthik [kskarthik at disroot dot org]
  * License:GPLv3
*/
'use strict'
// list all transaction details 
listTransactions();

function listTransactions()  {

for (let n=localStorage.length; n--; n>0) {

  if (localStorage.getItem('USR_TXN'+n)) {
 
let u, trow;

u = JSON.parse( localStorage.getItem("USR_TXN"+n) );
 
trow = document.createElement('tr');
  
 trow.innerHTML ="<th>" + n + "</th>" + "<td class='trx'>" + u.type + "</td>"+ "<td>" + u.coin + "</td>" + "<td>"+ u.val +"</td>" + "<td>" + u.qty + "</td>"+"<td>"+ u.date+ "</td>";

    document.querySelector('#table-body').appendChild(trow)

  }
 }
 colorify();
}

// Show transaction type in color
function colorify() {

let txn = document.querySelectorAll('.trx');

for (let i=0; i<txn.length; i++) {

  if (txn[i].textContent == "BUY") {

    txn[i].style.color = "blue";

  } else {

    txn[i].style.color = "red";

  }
 }
}