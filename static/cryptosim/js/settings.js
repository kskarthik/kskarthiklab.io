/*
  * Name: Crypto Sim
  * Author: Sai Karthik [kskarthik at disroot dot org]
  * License:GPLv3
*/
'use strict'

document.querySelector('#reset').onclick = reset;
document.querySelector('#export').onclick = usrExport;
document.querySelector('#import').onclick = usrImport;
document.querySelector('#help').onclick = function() {swal("Your data icludes your balance, wallets, transaction history. You can export your data to another device & continue playing cryptosim seamlessly!") };

// reset user data
function reset() {

swal({
  title: "Are you sure?",
  text: "Reset will erase all your data, This action is irreversable",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {

    localStorage.clear();

    swal({
      title: "Reset Successful !",
      icon: "success",
    });
  } else {

    swal("Reset terminated");
  }
});
}

let txt = document.querySelector('#storage'); 
// Export user's data
function usrExport() {
 
        txt.value = "";
          txt.value = JSON.stringify(localStorage); 
            setTimeout(txt.select(), 1000);
              document.execCommand("copy");
                txt.value = "";

  swal("Data copied to clipboard. Please paste it to a text file & save", {title: "Data Export successful!",icon: "success"} );
}

// Save user's data to localStorage
function usrImport() {

    let importedJson = JSON.parse(txt.value); 

    if ( (txt.value != "") && (importedJson.USER_BAL) ) {

      localStorage.clear();

      for (let i in importedJson) {
    
        localStorage.setItem(i, importedJson[i]);
       
      }

      txt.value = "";
        swal("Enjoy using CryptoSim !", {title: "Data import successful !",icon: "success"});
    
    } else {

      swal("Please check the data once", {title: "Invalid cryptosim data", icon: "error"});

    }
}