import subprocess
from sys import argv as args


"""
pip2poetry - Import your requirements.txt into poetry

This script assumes that poetry is already installed

USAGE:
    python3 py2poetry.py <optional file path>
"""


pkg_file = "requirements.txt"


def main():
    # open the file containing the packages & read the contents
    with open(pkg_file) as f:
        file = f.readlines()
        # loop through all the lines
        for line in file:
            # ignore commented and blank lines
            if not line.startswith("#") and not line.isspace():
                # strip white spaces & spaces between words
                pkg = line.strip().replace(" ", "")
                # install the package with poetry
                try:
                    subprocess.run(["poetry", "add", pkg])
                except:
                    print(f"Failed to install {pkg}")


if len(args) == 2:
    pkg_file = args[1]
    main()
else:
    main()
